# Module 01

## 1. Containers

### Comprehension Questions

- What is a container?
- What are the advantages of running software in containers over running software
    - locally on the machine/server?
    - in a VM?
- What do the following container-related terms mean
    - Image
    - Image layer
    - Registry
    - Tag
    - Volume
    - bind mount
    - Container EngineContainer Runtime
- You edit files inside of the container directly (not inside of a volume, not a bind-mounted directory). What happens with the changes after the container is stopped (`docker stop`)? What happens with the changes after the container is removed (`docker remove`)?
- What is a `Containerfile`/`Dockerfile` used for?
- What is container orchestration used for?
- Name some container orchestration tools

### Exercise 1.1.1

1. Install Docker on your operating system
2. Run a container with the Nginx web server in the background, use the [official NGINX image](https://hub.docker.com/_/nginx) from the Docker hub and verify it is running.
3. Expose port 80 so that you can access the served default page locally via a web browse
4. Start a new Nginx container, this time without any port being exposed. Now use a second container utilizing the https://hub.docker.com/r/curlimages/curl image and use it to get the page served by the Nginx container.
5. Serve a local `index.html` file (content does not matter, one line with a random string is sufficient) by mounting it into the container
6. Create a Dockerfile that uses the official Nginx image as the starting point, then copy the local index.html file into the image. Build the image, run a container from it, access the served web page again to verify everything works.

## 2. Git, GitLab, and GitHub

### Comprehension Questions

- Why version control?
- What is a repository?
- What is a branch?
- What is a tag?
- What is a merge request/pull request?
- Check out https://gitlab.com/wkerschbaumer/devops-tutorial and commit your solutions for Exercise 

### Exercise 1.2.1

1. Install git locally for your operating system via your OS's package manager or from https://git-scm.com/downloads
2. Additionally, install a graphical git client (e.g., see https://git-scm.com/downloads/guis)
3. Explore and analyze the repository for the Dockerfile of the official Nginx images on Docker hub: `https://github.com/nginxinc/docker-nginx` and try to answer the following questions (you can use either `git` or a graphical Git client to find out):
    1. What date is the first commit from?
    1. What is the latest tag?
    2. What is the commit hash of tag `1.21.3`
    3. What issue was fixed by commit `2b064090d1ebb724865ce7e6f4b6cc64c4c5cd6e`?
    4. How many remote branches do currently exist? What are their names and assumed purposes?
5. Create an account at https://gitlab.com, clone the repository https://gitlab.com/wkerschbaumer/devops-tutorial and commit the files from your solution of exercise 1.1.1 as pull request/merge request

