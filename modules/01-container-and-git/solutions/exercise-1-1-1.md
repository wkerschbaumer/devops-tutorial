Execise 1.1.1

## 1. Install Docker

Follow steps from https://docs.docker.com/get-docker/

## 2. Run a container with the Nginx web server in the background, use the [official NGINX image](https://hub.docker.com/_/nginx) from the Docker hub and verify it is running.

    docker run --name my-first-nginx-container -d nginx

    docker ps
    CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS     NAMES
    3656a5195900   nginx     "/docker-entrypoint.…"   34 seconds ago   Up 34 seconds   80/tcp    my-first-nginx-container

    curl localhost:80
    curl: (7) Failed to connect to localhost port 80: Connection refused

## 3. Expose port 80 so that you can access the served default page locally via a web browse.

    docker run --name my-first-nginx-container -d -p 8080:80 nginx
    docker: Error response from daemon: Conflict. The container name "/my-first-nginx-container" is already in use by container "3656a519590041fc34ae6a825905d41f56ee6d7ffdd9df251f4415e32615a129". You have to remove (or rename) that container to be able to reuse that name.
    See 'docker run --help'.

    docker rm my-first-nginx-container
    Error response from daemon: You cannot remove a running container 3656a519590041fc34ae6a825905d41f56ee6d7ffdd9df251f4415e32615a129. Stop the container before attempting removal or force remove

    docker stop my-first-nginx-container
    my-first-nginx-container

    docker rm my-first-nginx-container

    docker run --name my-first-nginx-container -d -p 8080:80 nginx
    f9bb9a3e84d0e7b4117f0316a603367fc2f0b8fc5feb1b1aa20ba165fc015fde
 
    From the Docker host:

    curl localhost:8080
    <!DOCTYPE html>
    <html>
    <head>
    <title>Welcome to nginx!</title>
    <style>
    html { color-scheme: light dark; }
    body { width: 35em; margin: 0 auto;
    font-family: Tahoma, Verdana, Arial, sans-serif; }
    </style>
    </head>
    <body>
    <h1>Welcome to nginx!</h1>
    <p>If you see this page, the nginx web server is successfully installed and
    working. Further configuration is required.</p>
    
    <p>For online documentation and support please refer to
    <a href="http://nginx.org/">nginx.org</a>.<br/>
    Commercial support is available at
    <a href="http://nginx.com/">nginx.com</a>.</p>
    
    <p><em>Thank you for using nginx.</em></p>
    </body>
    </html>

    From a remote host:

    curl $EXT_IP_ADDRESS_OF_DOCKER HOST:8080
    ... (seme result as above)

### 4. Start a new NGINX container, this time without any port being exposed. Now use a second container utilizing the https://hub.docker.com/r/curlimages/curl image and use it to get the page served by the Nginx container.

    docker run --name my-first-nginx-container -d nginx
    
    curl localhost:80
    curl: (7) Failed to connect to localhost port 80: Connection refused

    curl localhost:8080
    curl: (7) Failed to connect to localhost port 8080: Connection refused

    docker inspect --format '{{ .NetworkSettings.IPAddress }}'  my-first-nginx-container
    172.17.0.2

    docker run --rm curlimages/curl 172.17.0.2
    Unable to find image 'curlimages/curl:latest' locally
    latest: Pulling from curlimages/curl
    a0d0a0d46f8b: Pull complete
    8657f0e1413c: Pull complete
    52cb86f96cb7: Pull complete
    fcd03ebe1937: Pull complete
    9e39270bffa6: Pull complete
    a15e0345730b: Pull complete
    c1070d8102f9: Pull complete
    055d3ce1e3f2: Pull complete
    110e7f874674: Pull complete
    Digest: sha256:d588ff348c251f8e4d1b2053125c34d719a98ff3ef20895c49684b3743995073
    Status: Downloaded newer image for curlimages/curl:latest
      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100   615  100   615    0     0   463k      0 --:--:-- --:--:-- --:--:--  600k
    <!DOCTYPE html>
    <html>
    <head>
    <title>Welcome to nginx!</title>
    <style>
    html { color-scheme: light dark; }
    body { width: 35em; margin: 0 auto;
    font-family: Tahoma, Verdana, Arial, sans-serif; }
    </style>
    </head>
    <body>
    <h1>Welcome to nginx!</h1>
    <p>If you see this page, the nginx web server is successfully installed and
    working. Further configuration is required.</p>
    
    <p>For online documentation and support please refer to
    <a href="http://nginx.org/">nginx.org</a>.<br/>
    Commercial support is available at
    <a href="http://nginx.com/">nginx.com</a>.</p>
    
    <p><em>Thank you for using nginx.</em></p>
    </body>
    </html>    
     

    docker run --rm curlimages/curl my-first-nginx-container
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
    0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0curl: (6) Could not resolve host: my-first-nginx-container

    -> default docker bridge does not support automatic network resolution -> use a user-defined one:

    docker network create my-first-docker-network
    5bbb12493b90c922d7bc8f64a514101b835df5fb757fafdf41dd63f972f8b24f

    docker run --name my-first-nginx-container -d --network my-first-docker-network nginx

    docker run --rm --network my-first-docker-network curlimages/curl my-first-nginx-container
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
    100   615  100   615    0     0   140k      0 --:--:-- --:--:-- --:<!DOCTYPE html>
    <html>
    <head>
    <title>Welcome to nginx!</title>
    <style>
    html { color-scheme: light dark; }
    body { width: 35em; margin: 0 auto;
    font-family: Tahoma, Verdana, Arial, sans-serif; }
    </style>
    </head>
    <body>
    <h1>Welcome to nginx!</h1>
    <p>If you see this page, the nginx web server is successfully installed and
    working. Further configuration is required.</p>
    
    <p>For online documentation and support please refer to
    <a href="http://nginx.org/">nginx.org</a>.<br/>
    Commercial support is available at
    <a href="http://nginx.com/">nginx.com</a>.</p>
    
    <p><em>Thank you for using nginx.</em></p>
    </body>
    </html>
    --:--  150k

### 5. Serve a local `index.html` file (content does not matter, one line with a random string is sufficient) by mounting it into the container

    mkdir -p /var/www/docker-html
    echo 'Hello World!' > /var/www/docker-html/index.html
    docker run --name my-first-nginx-container --network my-first-docker-network -p 80:80 -v /var/www/docker-html:/usr/share/nginx/html:ro  -d nginx
    
    curl localhost
    Hello World!

### 6. Create a Dockerfile that uses the official NGINX image as the starting point, then copy the local index.html file into the image. Build the image, run a container from it, access the served web page again to verify everything works.

    # Create "Containerfile" with the following contents:

    FROM nginx:1.20.2
    COPY index.html /usr/share/nginx/html/index.html
   
    # then build an image:


    docker build -t my-first-nginx-image -f ./Containerfile .
    Sending build context to Docker daemon  3.072kB
    Step 1/2 : FROM nginx:1.20.2
    1.20.2: Pulling from library/nginx
    e5ae68f74026: Pull complete
    2dc3587e7d0c: Pull complete
    b8258363a4a3: Pull complete
    963807cfb489: Pull complete
    5faf54adf667: Pull complete
    07bd53fd2d21: Pull complete
    Digest: sha256:71a1217d769cbfb5640732263f81d74e853f101b7f2b20fcce991a22e68adbc7
    Status: Downloaded newer image for nginx:1.20.2
     ---> c90c369e7833
    Step 2/2 : COPY index.html /usr/share/nginx/html/index.html
     ---> 017571f9ead5
    Successfully built 017571f9ead5
    Successfully tagged my-first-nginx-image:latest


    docker image ls
    REPOSITORY             TAG       IMAGE ID       CREATED          SIZE
    my-first-nginx-image   latest    017571f9ead5   36 seconds ago   141MB
    nginx                  1.20.2    c90c369e7833   4 days ago       141MB
    
    docker run --name my-first-nginx-container --network my-first-docker-network -p 80:80 -d my-first-nginx-image

    curl localhost
    Hello World!