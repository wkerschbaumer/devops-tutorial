# Execise 1.2.1

## 3. Explore and analyze the repository for the Dockerfile of the official Nginx images on Docker hub: `https://github.com/nginxinc/docker-nginx` and try to answer the following questions (you can use either `git` or a graphical Git client to find out):

    git clone https://github.com/nginxinc/docker-nginx

### 1. What date is the first commit from?

    git log --reverse

    -> 2014-09-17


### 2. What is the latest tag?

    Latest tag in the current branch `master`:

    git describe --tags --abbrev=0
    
    Also sufficient to search git log for latest tag:

    git log


### 3. What is the commit hash of tag `1.21.3`

    git show 1.21.3

    git rev-list -n 1 1.21.3


    -> d496baf859613adfe391ca8e7615cc7ec7966621

### 4. What issue was fixed by commit `2b064090d1ebb724865ce7e6f4b6cc64c4c5cd6e`?

    git log 2b064090d1

    git show 2b064090d1

    -> "Introduced an entrypoint script to autotune the number of worker processes"

### 5. How many remote branches do currently exist? What are their names and assumed purposes?

local branches:

    git branch

remote branches:

    git branch -r
     origin/520-fix-wsl
     origin/HEAD -> origin/master
     origin/alpine-nginxorg
     origin/master
     origin/tune-worker-processes