# Module 02

## 1. SSH

### Comprehension Questions

- What is SSH used for? Bonus points for not only mentioning the most obvious use cases ;)
- Explain how SSH Authentication works. Only an overview, about five sentences should be enough.
- What is the most commonly used implementation of SSH used out there?
- How can commands executed via SSH be restricted
- Exlain the different SSH forwarding types:
    - local
    - remote
    - dynamic
- How can options for `ssh` commands be saved to avoid typing long commands all the time
- What are ways to handle 

### Exercise 2.1.1

- generate an SSH key
    - make sure that the type of key and the number of bits used in key-generation are considered secure from todays point of view
- SSH into the machine `test-01.devops-tutorial.unix.institute` with user `devops-tutorial` using your key
    - find out and write down kernel version and operating system of the remote host
    - what are the other public keys that are allowed to log in on this machine via SSH and the same user?
- What SSH-based possibities are there to copy ofer files to ar remote hosst? Use one of them for the past part of exercise 2.3.1

## 2. Git

[./resources/git-staging-workflow.png](./resources/git-staging-workflow.png)

Image source: https://blog.isquaredsoftware.com/2021/01/coding-career-git-usage/

### Comprehension Questions

- Explain every git stage and command in the image linked above in short. For better understanding, do the corresponding exercise 2.2.1 first.
- Find some universally valid best practices (independend of a specific worklfow a project/company might use). Use the blog post mentioned above as starting point

### Exercise 2.2.1

- Create a repository on your GitLab account, add some files to it, and try to use as many of the commands mentioned in the image above as possible
- If conflicts or errors arise that hinders you to move on with the exercise, just clone the repository again into a new directory, we will try to resovle the conflict together in the next totorial
