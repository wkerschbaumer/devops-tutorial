# Module 03

## 1. Docker Compose

### Exercise 3.1.1

- Set up a docker-compose file with a `postgres` database container, and a `pgadmin4` (Web-based Database administration tool) container
- the following requirements should be met:
    - the docker-compose application runs on `test-01.devops-tutorial.unix.institute`
        - log in with user `devops-tutorial`, password-less `sudo` is enabled.
        - you can either work directly on the remote host or develop the solution locally first and transfer it over once ready
        - you can use rootless Docker if you want but you don't have to
    - pgadmin is accessible publicly via HTTP on Port 80
    - the postgres container itself should NOT be accessible from outside of Docker
    - after logging into pgadmin, a connection to the postgres instance can be established and `SQL` commands against the database can be executed 
    - you can test and try without the docker compose first if it is easier
- Verify everything is working by accessing `pgadmin` via the Internet
- copy over and run docker-compose over to the server mentioned above, `sudo` without password is enabled, you don't have to use rootless Docker but you can if it works out of the box. Verify everything is working by accessing `pgadmin` via the Internet.

### Exercise 3.1.2

    - based on the solution of 3.1.1:
    - make sure the table described below gets created on the first `docker-compose` run, but don't override if it already exists.
    - make the pgadmin instance available on Port 443/HTTPS with a valid SSL certificate provided by Let's Encrypt, disable access on port 80

    CREATE TABLE accounts (
	user_id serial PRIMARY KEY,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	email VARCHAR ( 255 ) UNIQUE NOT NULL,
	created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP 
    );